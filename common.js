const firstPassField = document.querySelector("#first-field");
const secondPassField = document.querySelector("#second-field");
const passIcons = document.querySelectorAll(".icon-password");
const passForm = document.querySelector(".password-form");

passForm.addEventListener("click", (event) => {
  if (event.target.classList.contains("icon-password")) {
    const icon = event.target;
    const passInput = icon.parentNode.querySelector("input");
    const isVisible = passInput.type === "password";

    passInput.type = isVisible ? "text" : "password";
    icon.classList.toggle("fa-eye");
    icon.classList.toggle("fa-eye-slash");
  }
});

passForm.addEventListener("submit", (event) => {
  event.preventDefault(); // Потрібно для того щоб перешкодити стандартній поведінці браузера при "submit", після натискання якого сторінка не повинна перезавантажитись, а виконати перевірку

  const firstPassValue = firstPassField.value;
  const secondPassValue = secondPassField.value;

  if (firstPassValue === secondPassValue && firstPassValue !== "" && secondPassValue !== "") {
    alert("You are welcome");
    firstPassField.value = "";
    secondPassField.value = "";
    let existingError = document.querySelector(".error-message");
    if (existingError) {
      existingError.remove();
    }
  } else {
    let existingError = document.querySelector(".error-message");
    
    if (!existingError) {
      const ifError = document.createElement("p");
      ifError.textContent = "Потрібно ввести однакові значення";
      ifError.style.color = "red";
      ifError.classList.add("error-message");

      secondPassField.insertAdjacentElement("afterend", ifError);
    } 
  }
});